#include <stdio.h>


int main(void)
{
    char name[46];
    char initials[4];
    int len, c, j, i;
    
    for (i=0; (c=getchar()) != EOF; i++)
    {
        if (c != '\n')
        {   
            if (c >= 'a')
                c -= ('a' - 'A');
            name[i] = c;
            len++;
        }
        else
            break;
    }
   
    j = 0; 
    // check if first character is space
    if (name[0] != ' ')
    {
        initials[0] = name[0];
        j++;
    }

    // loop through characters and add character after space to initials
    for(i=0; i<len; i++)
    {
        if(name[i] == ' ' && name[i+1] != ' ')
        {
            initials[j] = name[i+1];
            j++;
        }
    }
    printf("%s\n", initials);
    return 0;
}
